<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::collection('jobs',
            function(\Jenssegers\Mongodb\Schema\Blueprint $collection) {
            $collection->index('name');
            $collection->index('alias');
        });

        Schema::collection('job',
            function(\Jenssegers\Mongodb\Schema\Blueprint $collection) {
            $collection->index('name');
            $collection->index('alias');
        });

        Schema::collection('skills',
            function(\Jenssegers\Mongodb\Schema\Blueprint $collection) {
            $collection->index('name');
            $collection->index('alias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
